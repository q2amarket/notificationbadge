package com.codepaints.notificationbadge;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    MenuItem notificationsMenuItem, jobsAppliedMenuItem;
    TextView notificationsBadgeCountText, jobsAppliedBadgeCountText;
    int pendingNotificationCount = 12;
    int pendingJobsAppliedCount  = 3;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ) {

        getMenuInflater().inflate( R.menu.notification_menu, menu );

        // getting menu items
        notificationsMenuItem = menu.findItem( R.id.nav_notification );
        jobsAppliedMenuItem = menu.findItem( R.id.nav_jobs_applied );

        // check for notifications
        if ( pendingNotificationCount == 0 ) {

            notificationsMenuItem.setActionView( null );

        } else {

            notificationsMenuItem.setActionView( R.layout.notifications_badge );
            View view = notificationsMenuItem.getActionView();
            notificationsBadgeCountText = view.findViewById( R.id.notifications_badge_text );
            notificationsBadgeCountText.setText( badgeCounter( pendingNotificationCount ) );

            view.findViewById( R.id.notifications_badge_layout ).setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick( View v ) {
                    Toast.makeText( MainActivity.this, "Notifications Clicked", Toast.LENGTH_SHORT ).show();
                }
            } );

        }

        // check for jobs applied
        if ( pendingJobsAppliedCount == 0 ) {

            jobsAppliedMenuItem.setActionView( null );

        } else {

            jobsAppliedMenuItem.setActionView( R.layout.jobs_applied_badge );
            View view = jobsAppliedMenuItem.getActionView();
            jobsAppliedBadgeCountText = view.findViewById( R.id.jobs_applied_badge_text );
            jobsAppliedBadgeCountText.setText( badgeCounter( pendingJobsAppliedCount ) );

            view.findViewById( R.id.jobs_applied_badge_layout ).setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick( View v ) {
                    Toast.makeText( MainActivity.this, "Jobs Applied Clicked", Toast.LENGTH_SHORT ).show();
                }
            } );

        }

        //setupBadge(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {

        switch ( item.getItemId() ) {
            case R.id.nav_notification:
                Toast.makeText( this, "Notification arrived", Toast.LENGTH_SHORT ).show();
        }

        return super.onOptionsItemSelected( item );
    }

    /**
     * Check if the number is grater than 99 and return string based on it.
     *
     */
    public String badgeCounter( int count ) {

        String totalCount;

        if ( count > 99 ) {
            totalCount = "...";
        } else {
            totalCount = String.valueOf( count );
        }

        return totalCount;

    }

    /*
     * Currently not in use
     */
    private void setupBadge( Menu menu ) {
        if ( notificationsBadgeCountText != null ) {

            if ( pendingNotificationCount == 0 ) {

                if ( notificationsBadgeCountText.getVisibility() != View.GONE ) {
                    notificationsBadgeCountText.setVisibility( View.GONE );
                }

            } else {
                notificationsBadgeCountText.setText( String.valueOf( Math.min( pendingNotificationCount, 99 ) ) );
                notificationsMenuItem = menu.findItem( R.id.nav_notification );
                View view = notificationsMenuItem.getActionView();
                notificationsBadgeCountText = view.findViewById( R.id.notifications_badge_text );

                if ( notificationsBadgeCountText.getVisibility() != View.VISIBLE ) {
                    notificationsBadgeCountText.setVisibility( View.VISIBLE );
                }
            }

        }
    }
}
